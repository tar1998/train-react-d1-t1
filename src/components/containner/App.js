import React, { Component } from 'react';
import '../../App.css';
import Search from '../view/Search'
import ResultSearch from '../view/ResultSearch';
class App extends Component {
  state = {
    mockData: [
      'React JS',
      'VScode',
      'HTML5',
      'JSX',
      'JSX',
      'JSX',
      'JSX'
    ],
    result: [],
    search: ''
  }
  handleOnchange = (event) => {
    let { name, value } = event.target
    this.setState({
      [name]: value // ['search']:value            
    })
  }
  searchSubmit = () => {
    let { mockData, search } = this.state
    let result = mockData.filter((value, index) => {
      return value === search
    })
    this.setState({
      result: result
    })
  }
  render() {
    let { search, result } = this.state
    return (
      <div>
        <Search
          search={search}
          handleOnchange={this.handleOnchange}
          searchSubmit={this.searchSubmit} />
        <ResultSearch ResultSearch={result} />
      </div>
    )
  }
}


// function getName() {
//   return "Bob"
// }
// let App = () => {
//   let name = "Harry Potter"

//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <h1 className="App-title">Welcome to React 5551</h1>
//       </header>
//       <p className="App-intro">
//         {
//           getName()
//         }
//       </p>
//     </div>
//   )
// }


export default App;
